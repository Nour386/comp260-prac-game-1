﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {


	//public float speed = 4.0f; //metres per second
	//public float turnSpeed = 180.0f; //degrees
	public Transform target;
	public Transform target2;
	private Transform currentTarget;
	public Vector2 heading = Vector3.right;

	//public parameters with default values
	public float minSpeed, maxSpeed;
    public float minTurnSpeed , maxTurnSpeed ;

    //public refernce for the death explosion particl sysetm
    public ParticleSystem Explosion;

	//private state
	private float speed;
	private float turnSpeed;

	// Use this for initialization
	void Start () {
		PlayerMove[] players = FindObjectsOfType<PlayerMove>();
		target = players[0].transform;
		target2 = players[1].transform;

		//bee initially moves in random direction
		heading = Vector2.right; 
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		//set speed and turn speed randomly
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);
	}


    void OnDestroy() {
        //create an aexplosion at the bee's current position
        ParticleSystem explosion = Instantiate(Explosion);
        explosion.transform.position = transform.position;
        //destory the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);
   
    }


	void OnDrawGizmos(){
		//draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		//draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay (transform.position, direction);
	}

	void Update (){

		var distance1 = Vector3.Distance(target.transform.position, transform.position);
		var distance2 = Vector3.Distance(target2.transform.position, transform.position);
		if (distance1 > distance2) {
			currentTarget = target2;
		} else {
			currentTarget = target;
		}

		//get the vector fomr the bee to the target 
		Vector2 direction = currentTarget.position - transform.position;

		//calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		//turn left or right
		if (direction.IsOnLeft (heading)) {
			//target on left, roata anticlockwise-to the left you pretencious littl-
			heading = heading.Rotate (angle);
		} else {
			//target on right, rotate clockwise, or "turn right"
			heading = heading.Rotate(-angle);
		}
			
		transform.Translate (heading * speed * Time.deltaTime);
}
}