﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {


	public int nBees = 50;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;

    //the buzz killer kill off bees within a certain radius
    public void DestroyBees(Vector2 centre, float radius)
    {
        for(int i = 0; i< transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            //bug corrected, added type converstion
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }


	// Use this for initialization
	void Start () {
		//create bees
		for (int i = 0; i < nBees; i++) {
			//instansiate bee
			BeeMove bee = Instantiate (beePrefab);

			//attach to this object in the hierhary
			bee.transform.parent = transform;
			//give the bee a name and a number
			bee.gameObject.name = "Bee " + i;

			//move the bee to random position within the bounding rectanlge
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
