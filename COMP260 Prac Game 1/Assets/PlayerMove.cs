﻿using UnityEngine;
using System.Collections;


public class PlayerMove : MonoBehaviour {
    private BeeSpawner beeSpawner;//make refernce to beespawner code

	// Use this for initialization
	void Start () {
        beeSpawner = FindObjectOfType<BeeSpawner>();
	}

    public float destroyRadius = 1.0f;
	public float maxSpeed = 5.0f;

	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1"))
        {
            //destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }
        
        //get the axis values
		if (gameObject.tag == "Player") {
			movePlayer1 ();
		} else if(gameObject.tag == "Player2") {
			movePlayer2 ();
		}

	}



	public void movePlayer1()
	{
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal"); // between -1 and 1: 0 is stopped, -1 is left, 1 is right
		direction.y = Input.GetAxis ("Vertical"); // between -1 and 1: 0 is stopped, -1 is down, 1 is up
		//scale the velocity by the frame duration
		Vector2 velocity = direction * maxSpeed;

		//move the object
		transform.Translate (velocity * Time.deltaTime);
	}

	public void movePlayer2()
	{
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal2"); // between -1 and 1: 0 is stopped, -1 is left, 1 is right
		direction.y = Input.GetAxis ("Vertical2"); // between -1 and 1: 0 is stopped, -1 is down, 1 is up
		//scale the velocity by the frame duration
		Vector2 velocity = direction * maxSpeed;

		//move the object
		transform.Translate (velocity * Time.deltaTime);
	}
}
